#include "hex_viewer_widget.h"
#include "ui_hex_viewer_widget.h"


hex_viewer_widget::hex_viewer_widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hex_viewer_widget)
{
    ui->setupUi(this);

    hexview = new QHexView();
    hexview->show();


}

hex_viewer_widget::~hex_viewer_widget()
{
    delete hexview;
    delete ui;
}
