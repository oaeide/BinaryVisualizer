/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <string.h>
#include <vector>
#include "task_data_plotter.h"
#include "plotter.h"
//#define LOCKLESS_ADD
#define simd_add
Task_data_plotter::Task_data_plotter() : Task("Task data plotter")
{
	data = NULL;
}

Task_data_plotter::~Task_data_plotter()
{
	if(data)
		free(data);
}

void Task_data_plotter::set_properties(struct filedatabuffer *fdb, int x, int y, struct displaydata* display_data, float add_value)
{

	this->data_length = fdb->bytes_in_buffer;
	this->alignment_max_value = fdb->alignment_max_value;
	this->alignment_scaling = fdb->alignment_scaling;
	this->data = (unsigned char*)malloc(data_length*sizeof(unsigned char));
	memcpy(this->data, fdb->buffer, data_length);

	this->buffer = *fdb;
	buffer.buffer = this->data;
	buffer.filep = NULL;
	this->x = x;
	this->y = y;
	this->display_data = display_data;
	this->add_value = add_value;
}

void Task_data_plotter::execute(Task_manager *manager)
{
#ifdef simd_add
	std::vector<int> storage;
#endif
	union{
		int val;
		struct{ unsigned char w, x, y, z; };
	}chars = {0};
	chars.y = x;
	chars.x = y;
	long long normalized_value;
	while((normalized_value = get_next_scaled_value_from_filedatabuffer(&buffer)) >= 0){
		chars.val = (chars.val | normalized_value) << 8;
#ifdef LOCKLESS_ADD
		add_point_to_displaydata_lockfree(display_data, add_value, chars.x, chars.y, chars.z);
#endif
#ifdef simd_add
		//Read into a vector. This will run along cache line and be near free
		storage.push_back(chars.z*256*256 + chars.y*256 + chars.x);
#else
		add_point_to_displaydata(display_data, add_value, chars.x, chars.y, chars.z);
#endif
	}

	std::vector<uint8_t> bit_array(256*256*256, 0);
#ifdef simd_add
	//Lock the entire displaydata structure and write all the values from storage into displaydata using SIMD
	//Going through storage will be near free, as it will be continious along cache lines. Adding to displaydata will be the
	//expensive part as it will be random I/O with no caching.
	int size = storage.size();


#pragma omp simd
	for(int n = 0; n < size; ++n){
		if((++(bit_array[storage[n]])) == 255){
#ifdef _WIN32
			EnterCriticalSection(&(display_data->vertices.point_mutex[0]));
#else
			display_data->vertices.point_mutex[0].lock();
#endif
			display_data->vertices.points[storage[n]].count += bit_array[storage[n]]*add_value;
			bit_array[storage[n]] = 0;
#ifdef _WIN32
			LeaveCriticalSection(&(display_data->vertices.point_mutex[0]));
#else
			display_data->vertices.point_mutex[0].unlock();
#endif
		}
	}

#ifdef _WIN32
	EnterCriticalSection(&(display_data->vertices.point_mutex[0]));
#else
	display_data->vertices.point_mutex[0].lock();
#endif
#pragma omp simd
	for(int n = 0; n < 256*256*256; ++n)
		display_data->vertices.points[n].count += bit_array[n]*add_value;
#ifdef _WIN32
	LeaveCriticalSection(&(display_data->vertices.point_mutex[0]));
#else
	display_data->vertices.point_mutex[0].unlock();
#endif
#endif
}
