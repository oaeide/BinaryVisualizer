/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "libtask/task.h"
#include "alignment_tables.h"
#include "plotter.h"

#ifndef TASK_BYTE_PLOTTER_H
#define TASK_BYTE_PLOTTER_H

class Task_byte_plotter : public Task
{
public:
	Task_byte_plotter();
	~Task_byte_plotter();
	void set_properties(struct data_container* data_cont, struct filedatabuffer* fdb, long long start_offset);
	void execute(Task_manager* manager);
private:
	void init_filedatabuffer(alignment_round* alignment, long long max_value, int scaling);
	struct data_container data_cont;
	unsigned long long int data_length;
	unsigned char* data;
	struct filedatabuffer buffer;
	long long alignment_max_value;
	int alignment_scaling;
	long long start_offset;
};

#endif // TASK_BYTE_PLOTTER_H
