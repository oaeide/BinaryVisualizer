/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "auxilary_functioons.h"
#include "error_messages.h"
#include <QString>
#include <stdio.h>
#include <math.h>
#include <QSettings>

QString formatBytes(unsigned long int bytes)
{

    char tmp[128] = "";
    const char *si_prefix[] = { "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB" };
    const int base = 1024;
    int c = std::min((int)(log((double)bytes)/log((double)base)), (int)sizeof(si_prefix) - 1);
    sprintf(tmp, "%1.2f %s", bytes / pow((double)base, c), si_prefix[c]);
    return QString(tmp);
}

long int window_size_to_thousandth(int size) {
    return size / 1000;
}

long int offset_from_thousandths(int thousandths, long int filesize) {
    float tmp = (float) filesize * (float) thousandths / 1000;
    INFO("Thousands: %d, filesize: %ld, result: %f \n", thousandths, filesize, tmp);
    long int result = static_cast<long int>(tmp);
    return (long int) result;
}
