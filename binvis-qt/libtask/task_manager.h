/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H
#include <list>
#include <mutex>
#include <thread>
#include "task.h"
#include "task_consumer.h"
#include "tasks/task_idle.h"
#include "tasks/task_maintenance_cleanup.h"

class Task_consumer;
/**
 * @brief The Task_manager class.
 * This class handles the execution and management of tasks
 */
class Task_manager
{
public:
	friend class Task_consumer;
	/**
	 * @brief Task_manager
	 * @param threads The number of threads to be used for task execution
	 */
	Task_manager(int threads);
	~Task_manager();
	/*
	 * When stopping manager, only high priority tasks will be executed
	 */
	/**
	 * @brief add_task Adds a task to the pool of tasks to be executed
	 * @param t Task to be added
	 * @param high_priority If high_priority is set to high the task is added to the front of the task queue for execution. If Task_manager::stop() is called - high priority tasks are finished before stopping.
	 * Performance warning: add_task() uses a c++11 std::mutex, which on some platforms (windows) is implemented guaranteed to cause a context switch.
	 * On most other OSes this is implemented as a combination of spinlock and mutex, causing this not to cause a context switch if mutex is available.
	 */
	virtual void add_task(Task* t, bool high_priority = false);
	/**
	 * @brief more_tasks_available
	 * @return True if there are more tasks available for execution
	 */
	virtual bool more_tasks_available();
	/**
	 * @brief tasks_available
	 * @return the number of tasks available for execution
	 */
	virtual size_t tasks_available();
	/**
	 * @brief get_task Removes a task from list of tasks to be executed and returns this task. Returns a Task_idle if no tasks are executable.
	 * @return Task to be executed
	 */
	virtual Task* get_task();
	/**
	 * @brief is_running
	 * @return true if stop() has not been called
	 */
	virtual bool is_running();
	/**
	 * @brief when this has been called - is_running will return false. This will not return untill all worker threads have terminated.
	 */
	virtual void stop();
	/**
	 * @brief get_executed_task_count
	 * @return the number of tasks executed since creation.
	 */
	unsigned long get_executed_task_count();
	/*
	 * Deletes tasks marked for deletion which no tasks have dependencies to
	 */
	void perform_maintenance();
protected:
	void add_task_for_deletion(Task* t);
	std::list<Task*> tasks;
	std::list<Task*> to_be_deleted;
	void lock_tasks();
	void unlock_tasks();
	void lock_task_deletion();
	void unlock_task_deletion();
	std::mutex task_mutex;
	std::mutex delete_mutex;

	std::list<Task_consumer*> consumers;
	std::list<std::thread*> consumer_threads;
	Task* idle_task;
	Task* maintenance, *maintenance_timer;
	bool running;
	unsigned long executed_tasks;
};

#endif // TASK_MANAGER_H
