/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef PARTFILE_CONTROLLER_H
#define PARTFILE_CONTROLLER_H

#include "displaydata.h"
#include "plotter.h"

class partfile_controller
{
public:
    partfile_controller();
    ~partfile_controller();
    void set_filename(const char* filename);
    bool set_buffer_offsets(long int new_start, long int new_stop, displaydata* display_data);
    void set_currently_active_buffer_offsets(long int start, long int stop);
    void set_bit_alignment(int alignment, displaydata *display_data);
    void set_point_scaling(int scaling, displaydata *display_data);
    long long set_initial_file(const char *filename, displaydata *display_data);
    struct plotter_settings settings;
    struct data_container data;
private:
    void clear_data(displaydata *display_data);
    long int start_offset, stop_offset;
    int current_bit_alignment;
    int current_point_scaling_mode;
    char* filename;
};

#endif // PARTFILE_CONTROLLER_H
